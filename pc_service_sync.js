import { Parser } from 'https://esm.sh/htmlparser2?pin=v55';
import dateFormat from 'https://cdn.skypack.dev/dateformat';
import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";


const clientId = "pcservice"
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const mainUrl = "https://www.pcservice.com.uy/rest";
const loginPath = "/auth/login";

const tokenUsername = Deno.env.get("PCSERVICE_TOKEN_USERNAME");
const tokenPassword = Deno.env.get("PCSERVICE_TOKEN_PASSWORD");
const timeIntervals = Deno.env.get("PCSERVICE_INTERVALS") ?? 16

async function getAuthenticationToken(url, user, pass) {
  if (!url || !user || !pass) return null;
  try {
    let response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: user,
        password: pass,
      }),
    });
    let tokenJson = await response.json();

    return tokenJson.token;
  } catch (e) {
    return `Error processing token ${e.message}`;
  }
}


function decodeHtml(htmlContent) {
  let textToReturn = "";

  const parser = new Parser(
    {
      ontext(text) {
        textToReturn += text;
      },
      onclosetag(tagname) {
        if (tagname === "br") {
          textToReturn += '\n'
        }
      }
    },
    { decodeEntities: true }
  );
  parser.write(htmlContent);
  parser.end();

  textToReturn = textToReturn.replaceAll(
    "Siga Instrucciones de Registro aquí >>",
    ""
  );
  textToReturn = textToReturn.replaceAll("Link del Fabricante", "");
  let linkRegex =
    /((http?|https|ftp|file):\/\/)?((W|w){3}.)?[a-zA-Z0-9]+\.[a-zA-Z]+/gi;
  textToReturn = textToReturn.replaceAll(linkRegex, "");

  return textToReturn;
}

async function getArticlesByDate(url, authToken) {
  try {
    let response = await axiod.get(url, 
      {
        timeout: 150000,
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${authToken}`
        },
      })
    let jsonData = await response.data;
    return jsonData;
  } catch (e) {
    throw `Error processing articles: ${e.message}`;
  }
}

async function processArticles(articles) {
  for (let article of articles) {
    try {
      let payload = {
        sku: article.sku.trim(),
        client_id: clientId,
        integration_id: clientIntegrationId,
        ecommerce: [
          {
            ecommerce_id: "pcservice",
            options: {
              override_create: {
                "send": false
              },
              override_update: {
                "send": false
              },
            },
            properties: [
              { "name": article.description.trim() },
              { "description": decodeHtml(article.body) },
              { "stock": article.availability.stock ?? 0 },
              {
                "price": {
                  "value": article.price.price ?? 0,
                  "currency": article.price.currency
                },
              },
              {
                "images": article.images.map(elem => {
                  let imageVariants = elem.variations
                  let image = null
                  image = imageVariants.find(x => x.size === "MEDIUM")
                  if (image) {
                    return [image.url]
                  }
                  image = imageVariants.find(x => x.size === "LARGE")
                  if (image) {
                    return [image.url]
                  }
                  image = imageVariants.find(x => x.size === "SMALL")
                  if (image) {
                    return [image.url]
                  }
                  return []
                }).flat()
              },
              {
                "metadata": {
                  "id": `${article.id}`,
                  ...(article.categories && { "category_id": `${article.categories?.[0]?.id ?? null}` }),
                  ...(article.categories && { "sub_category_id": `${article.categories?.[0]?.childs?.[0]?.id ?? null}` }),
                  ...(article.extraData.barcode && article.extraData.barcode != "00000000000000" && { "EAN": article.extraData.barcode }),
                  ...(article.extraData.pvp_ecommerce && { "pvp_ecommerce": `${article.extraData.pvp_ecommerce}` }),
                  ...(article.extraData.pvp_ecommerce && { "pvp_ecommerce_iva": `${Math.round(((Number(article.extraData.pvp_ecommerce) * Number(1.22)) + Number.EPSILON) * 100) / 100}` }),
                  ...(article.extraData.currency && { "pvp_currency": `${article.extraData.currency}` }),
                  "availability": `${article.availability.availability}`,
                }
              }
            ],
            variants: []
          }
        ]
      }
      await sagalDispatch(payload)
    } catch (e) {
      console.log(`Error iterating item ${article.sku}: ${e.message}`)
    }
  }
}


async function init() {
  console.log(`Start pcservice integration for ${clientId}`)
  const intervals = Number(timeIntervals);
  const intervalLength = 3
  let ranges = new Array(intervals);
  for (let j = 0; j < intervals; j++) {
    let date = new Date()
    date.setHours(date.getHours() - (intervalLength * j));
    ranges[j] = date
  }
  for (let i = intervals - 1; i > 1; i--) {
    let from = dateFormat(ranges[i], "yyyymmddHHMMss")
    let to = dateFormat(ranges[i - 1], "yyyymmddHHMMss")
    console.log(`Get pcservice articles from  ${from} to ${to}`)
    try {

      const productPath = `/products/bydate/?from=${from}&to=${to}`

      let authToken = await getAuthenticationToken(
        mainUrl + loginPath,
        tokenUsername,
        tokenPassword
      );

      let articles = await getArticlesByDate(
        mainUrl + productPath,
        authToken
      );
      await processArticles(articles)
    } catch (e) {
      console.log(`Could not get pcservice articles from  ${from} to ${to}`)
    }
  }
}


await init()
