import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const apiUrl = Deno.env.get("INTEGRATOR_API");

const defaultStock = Deno.env.get("PCSERVICE_DEFAULT_STOCK_IF_AVAILABLE")

async function fetchArticles() {
    console.log('Fetching products...')
    let productsResponse = await axiod.get(`${apiUrl}/api/client/pcservice/ecommerce/pcservice/products`);
    let productsSkus = Object.keys(productsResponse.data)
    console.log(`Products Fetched: ${productsSkus.length}`)
    for (let productSku of productsSkus) {
        try {
            let syncCategory = true
            let producResponse = await axiod.post(`${apiUrl}/api/client/pcservice/ecommerce/pcservice/products`, { sku: productSku });
            let product = producResponse.data
            let product_properties = product.ecommerce[0].properties

            let name_obj = product_properties.find(x => x.hasOwnProperty("name"))
            let description_obj = product_properties.find(x => x.hasOwnProperty("description"))
            let images_obj = product_properties.find(x => x.hasOwnProperty("images"))
            let stock_obj = product_properties.find(x => x.hasOwnProperty("stock"))
            let metadata_obj = product_properties.find(x => x.hasOwnProperty("metadata"))

            let id = metadata_obj.metadata.id
            let availability = metadata_obj.metadata.availability
            let ean = metadata_obj.metadata.EAN
            let pvp_iva = metadata_obj.metadata.pvp_ecommerce_iva
            let pvp_currency = metadata_obj.metadata.pvp_currency

            if (defaultStock) {
                stock_obj.stock = Number(defaultStock)
            }

            if (availability === "false") {
                stock_obj.stock = 0
            }

            let price = null

            if (pvp_iva && pvp_currency) {
                price = {
                    "price": {
                        "currency": pvp_currency,
                        "value": Number(pvp_iva)
                    }
                }
            }

            let article = {
                sku: product.sku.trim(),
                client_id: clientId,
                integration_id: clientIntegrationId,
                ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                    return {
                        ecommerce_id: ecommerce_id,
                        options: {
                            ...(!syncCategory && { "override_create": { "send": false } })
                        },
                        properties: [
                            name_obj,
                            description_obj,
                            images_obj,
                            stock_obj,
                            ...((pvp_iva && pvp_currency) ? [price] : []),
                            {
                                "metadata": {
                                    "id": id
                                }
                            },
                            {
                                attributes: {
                                    ...(ean && { 'Código universal de producto': ean })
                                }
                            }
                        ],
                        variants: [],
                    }
                })
            }
            await sagalDispatch(article)
        } catch (e) {
            console.log(`Could not process ${productSku} -> error: ${e.message}`)
        }
    }

}

await fetchArticles()