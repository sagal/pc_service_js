# Integration scripts for system: PC Service

## Introduction

These scripts were created to run in the generic integration platform that links Sagal's clients' various forms of sending data into payloads that the business platform can understand. PC Service is a rest service that many clients use to deliver their data, exposing products ordered by their categories.

## Workflow

### Category Script (pc_service_categories.js)

- Retrieves all parent categories belonging to the client through PC Service's "(...)/rest/categories" API endpoint.
- Retrieves all the parent categories' children categories through PC Service's "(...)/rest/categories/{parentCategoryId}" endpoint.
- Syncs all the categories by uploading all the newly added categories to a Dynamo Database in AWS, where they can be referenced by the article integration script. Any categories that are found in the Dynamo Database, but not the client's database are thusly disabled.

### Article Script (pc_service_articles.js)

- Takes the client's registered categories by accessing the Dynamo Database, storing them in memory.
- For every category, fetches the client's registered articles from the PC Service API and sends them one by one for processing.
- During processing, the article's image, pricing and stock data is retrieved by the API and used to create the final processed article that is dispatched to Sagal.
- Finally, Sagal's existing database for the client's articles is compared with the currently active articles processed from PC Service. Any articles that are now missing from PC Service but extant in the database are disabled.

## Environment Variables

- **CLIENT_ID**: Sagal's known identifier for the client. A number.
- **CLIENT_INTEGRATION_ID**: the integration script's id in the existing runtime environment. A number.
- **CLIENT_ECOMMERCE**: a map holding this client's known identifiers for each E-Commerce service. (e.g: Key MELI for Mercadolibre)
- **PCSERVICE_URL**: the URL for the exposed endpoint for REST requests to the PCService client.
- **PCSERVICE_LOGIN_PATH**: the URL path needed to log into the PCService client and retrieve an authorization token.
- **PCSERVICE_TOKEN_USERNAME**: the username needed to obtain an authorization token.
- **PCSERVICE_TOKEN_PASSWORD**: the password needed to obtain an authorization token.
- **PCSERVICE_ARTICLE_MAX_AMOUNT**: The maximum number of articles to retrieve.
- **PCSERVICE_SKIP_PRICES**: Flag that determines whether prices should be skipped when retrieving the articles.
- **PCSERVICE_ARTICLE_STOCK_IF_AVAILABLE_AMOUNT**: The default amount of stock if an available article does not have it initialized.
- **PCSERVICE_HTML_TO_TEXT**: Flag that marks whether the retrieved data is HTML that needs to be parsed to text.
- **PCSERVICE_IMAGE_SIZE**: The image size, can be SMALL, MEDIUM or LARGE. Default value is MEDIUM, it is optional.
- **CATEGORIES_DYNAMO_TABLE_NAME**: The name of the table containing the client's enabled categories in Dynamo.
- **AWS_ACCESS_KEY_ID**: The id for the first AWS access key, needed to access Dynamo.
- **AWS_SECRET_ACCESS_KEY**: The secret AWS access key, needed to access Dynamo.
- **AWS_REGION**: The region of the AWS server where the Dynamo DB is stored.


## Client Endpoints

- "https://www.pcservice.com.uy/rest/auth/login": The path needed to obtain the Authorization Token.
- "https://www.pcservice.com.uy/rest/categories": The path needed to obtain all parent categories.
- "https://www.pcservice.com.uy/rest/categories/{parentCategoryId}": The path needed to obtain all the subcategories of a parent category.
- "https://www.pcservice.com.uy/rest/categories/{parentCategoryId}/{childCategoryId}/products": The path needed to obtain all the products for a given subcategory.
- "https://www.pcservice.com.uy/rest/products/{productId}": The path needed to obtain more details on a specific article.
- "https://www.pcservice.com.uy/rest/products/{productId}/price-stock": The path needed to obtain the article's pricing and stock data.

## Functions

### Category Script (pc_service_categories.js)

#### getAuthenticationToken(String url, String user, String pass)

Returns the JWT authenticator/authorizer needed to successfully make requests to the PC Service API, also acts as an identifier for the client in the PC Service API.

#### updateCategories()

Retrieves the available categories from the PC Service API and calls the function to upload them to the database.

#### uploadCategories(Object categoryMap)

For every category, calls the function to upload them to the database in Dynamo.

#### uploadCategoryToDynamo(Object categoryParams)

Inserts the category to Dynamo utilizing the dynamodb Deno library if it does not already exist on the database.

#### getAllParentCategories()

Retrieves all of the main categories the client is subscribed to, from the PCService API.

#### getChildCategories(String parentCategoryId)

Retrieves the main categories' subcategories, also called children categories, from the PCService API.

#### syncCategories(Object categoryMap)

Synchronizes the categories in the PCService API and the local database by deactivating all categories found in the local database but missing in PCService, and reactivating all categories missing in the local database's enabled categories.

#### fetchExistingCategories()

Retrieves all enabled categories from the local Dynamo database.

#### queryCategoriesInDynamo()

Performs the query that retrieves all enabled categories.

#### deactivateCategoryInDynamo(String identifier)

Updates the enabled category in Dynamo by marking it as disabled, using the dynamodb Deno library.

#### reactivateCategoryInDynamo(String identifier)

Updates the disabled category in Dynamo by marking it as enabled, using the dynamodb Deno library.

### Article Script (pc_service_articles.js)

#### getAuthenticationToken(String url, String user, String pass)

Returns the JWT authenticator/authorizer needed to successfully make requests to the PC Service API, also acts as an identifier for the client in the PC Service API.

#### fetchExistingCategories()

Retrieves all enabled categories from the local Dynamo database.

#### queryCategoriesInDynamo()

Performs the query that retrieves all enabled categories.

#### processAllArticlesInAllCategories(Object parentCategories)

For every category, calls a function to retrieve and process all articles for that category.

#### getAndProcessArticlesInCategory(String parentId, Object category)

Retrieves and processes all articles in a given category passed as a parameter by calling one function for each task.

#### getArticlesInCategory(String parentId, Object category)

Retrieves the articles in the PCService API for a given category and returns them as json.

#### processArticlesInCategory(Object category)

For every article in a given category, calls a function that processes the article.

#### processArticle(Object article, Number categoryPriceIncrease)

Processes the article by retrieving its full image, stock and pricing data through two requests to the PC Service API, and then applying a price increase to all prices according to the client's configurations. The article is restructured and dispatched to Sagal using the sagalDispatch function, and the identifier of the processed article is stored on a unique Set.

#### deactivateMissingSagalArticles(Set realSkus, String currentEcommerce)

Calls a function that retrieves the identifiers for all articles currently existing in the local database, and then compares them to the identifiers of all processed articles in the unique Set realSkus. Missing identifying skus not found within the processed Set are queued for deactivation through the deactivateArticle function.

#### getCurrentSkus(String currentEcommerce)

Retrieves the sku identifiers of all articles currently existing in the local database through an API request, for a specific client and e-commerce.

#### deactivateArticle(String sku)

Deactivates the article by marking it as without stock for every existing e-commerce registered to the client.

## Constraints

- When loading or processing a field, any tabs or spaces must be removed.
