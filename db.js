import { Client } from "https://deno.land/x/mysql/mod.ts";

const dbHost = Deno.env.get("PCSERVICE_DB_HOSTNAME");
const dbUsername = Deno.env.get("PCSERVICE_DB_USERNAME");
const dbPassword = Deno.env.get("PCSERVICE_DB_PASSWORD");


async function openDbConnection(callback) {
    const client = await new Client().connect({
        db: "pc_service",
        hostname: dbHost,
        username: dbUsername,
        poolSize: 3,
        password: dbPassword,
    });

    await callback(client);
    await client.close();
}

async function upsertCategory(client, category, subcategory) {
    await client.execute(
        `INSERT INTO pc_service.category (category_external_id, sub_category_external_id, category_title, sub_category_title, created_at, updated_at)
        SELECT ${category.id}, ${subcategory.id}, '${category.title.trim()}', '${subcategory.title.trim()}', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP  FROM DUAL 
        WHERE NOT EXISTS (SELECT * FROM pc_service.category WHERE category_external_id = ${category.id} AND sub_category_external_id = ${subcategory.id});
        `)
}

async function getClientCategory(client, categoryId, subcategoryId, clientId) {
    let rows = await client.execute(
        `SELECT cc.* FROM pc_service.category_client cc JOIN pc_service.category c ON c.id = cc.category_id 
            WHERE c.category_external_id = ${categoryId} 
            AND c.sub_category_external_id = ${subcategoryId} 
            AND cc.client_id = '${clientId}'
            LIMIT 1;
        `)
        return rows
}

export {
    upsertCategory,
    getClientCategory,
    openDbConnection
}