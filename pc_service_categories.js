
import { openDbConnection, upsertCategory } from './db.js'

const tokenUsername = Deno.env.get("PCSERVICE_TOKEN_USERNAME");
const tokenPassword = Deno.env.get("PCSERVICE_TOKEN_PASSWORD");
const mainUrl = "https://www.pcservice.com.uy/rest";


async function getAuthenticationToken(url, user, pass) {
    if (!url || !user || !pass) return null;
    try {
        let response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                username: user,
                password: pass,
            }),
        });
        let tokenJson = await response.json();

        return tokenJson.token;
    } catch (e) {
        console.log(`Error processing token ${e.message}`);
        throw e;
    }
}

async function getCategories() {
    try {
        let parentCategories = await getAllParentCategories();

        let parentCategoriesWithChildren = await Promise.all(
            parentCategories.map(getChildCategories)
        );

        return parentCategoriesWithChildren;
    } catch (e) {
        console.log(`Failed to update categories: ${e.message}`);
        return []
    }
}


async function getAllParentCategories() {
    let authToken = await getAuthenticationToken(
        mainUrl + "/auth/login",
        tokenUsername,
        tokenPassword
    );

    let response = await fetch(mainUrl + "/categories/", {
        method: "GET",
        headers: {
            Authorization: `Bearer ${authToken}`,
        },
    });
    let result = await response.json()
    return result
}

async function getChildCategories(parentCategory) {
    let authToken = await getAuthenticationToken(
        mainUrl + "/auth/login",
        tokenUsername,
        tokenPassword
    );

    let response = await fetch(mainUrl + `/categories/${parentCategory.id}`, {
        method: "GET",
        headers: {
            Authorization: `Bearer ${authToken}`,
        },
    });
    let result = await response.json()
    return result
}

async function init() {
    console.log("Start category integration")
    let resultingCategories = await getCategories();

     openDbConnection(async (client) => {
        for (let category of resultingCategories) {
            for (let subcategory of category.childs) {
                await upsertCategory(client, category, subcategory)
            }
        }
    })

}


await init()