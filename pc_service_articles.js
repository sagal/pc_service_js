import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";
import { getClientCategory, openDbConnection } from "./db.js"

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));

const apiUrl = Deno.env.get("INTEGRATOR_API");

const defaultStock = Deno.env.get("PCSERVICE_DEFAULT_STOCK_IF_AVAILABLE")
const priceIncrease = Deno.env.get("PCSERVICE_ARTICLE_PRICE_INCREASE") ?? 0;
const priceMultiplier = Deno.env.get("PCSERVICE_ARTICLE_PRICE_MULTIPLIER") ?? 1;
const manualCategories = Deno.env.get("PCSERVICE_MANUAL_CATEGORIES");


async function fetchArticles() {
    console.log('Fetching products...')
    let productsResponse = await axiod.get(`${apiUrl}/api/client/pcservice/ecommerce/pcservice/products`);
    let productsSkus = Object.keys(productsResponse.data)
    console.log(`Products Fetched: ${productsSkus.length}`)
    await openDbConnection(async (client) => {

        for (let productSku of productsSkus) {
            try {
                let syncCategory = true
                let categoryMap = {}
                let producResponse = await axiod.post(`${apiUrl}/api/client/pcservice/ecommerce/pcservice/products`, { sku: productSku });
                let product = producResponse.data
                let product_properties = product.ecommerce[0].properties

                let name_obj = product_properties.find(x => x.hasOwnProperty("name"))
                let description_obj = product_properties.find(x => x.hasOwnProperty("description"))
                let images_obj = product_properties.find(x => x.hasOwnProperty("images"))
                let stock_obj = product_properties.find(x => x.hasOwnProperty("stock"))
                let price_obj = product_properties.find(x => x.hasOwnProperty("price"))
                let metadata_obj = product_properties.find(x => x.hasOwnProperty("metadata"))

                let id = metadata_obj.metadata.id
                let category_id = metadata_obj.metadata.category_id
                let sub_category_id = metadata_obj.metadata.sub_category_id
                let availability = metadata_obj.metadata.availability
                let ean = metadata_obj.metadata.EAN


                if (manualCategories && manualCategories == "true") {
                    if (!categoryMap?.[category_id]?.[sub_category_id]) {
                        categoryMap[category_id] = {}
                        categoryMap[category_id][sub_category_id] = await getClientCategory(client, category_id, sub_category_id, clientId)
                    }
                    let clientCategory = categoryMap[category_id][sub_category_id]
                    if (!clientCategory.rows.length || (clientCategory.rows.length && clientCategory.rows[0].enabled == 0)) {
                        syncCategory = false
                    }
                }

                let prevStock = stock_obj.stock;

                if (defaultStock) {
                    stock_obj.stock = Number(defaultStock)
                }

                if (availability === "false" || !prevStock) {
                    stock_obj.stock = 0
                }

                price_obj.price.value = Math.round(((price_obj.price.value * Number(priceMultiplier)) + Number(priceIncrease) + Number.EPSILON) * 100) / 100


                let article = {
                    sku: product.sku.trim(),
                    client_id: clientId,
                    integration_id: clientIntegrationId,
                    ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                        return {
                            ecommerce_id: ecommerce_id,
                            options: {
                                ...(!syncCategory && { "override_create": { "send": false } })
                            },
                            properties: [
                                name_obj,
                                description_obj,
                                images_obj,
                                stock_obj,
                                price_obj,
                                {
                                    "metadata": {
                                        "id": id,
                                    }
                                },
                                {
                                    attributes: {
                                        ...(ean && { 'Código universal de producto': ean })
                                    }
                                }
                            ],
                            variants: [],
                        }
                    })
                }
                await sagalDispatch(article)
            } catch (e) {
                console.log(`Could not process ${productSku} -> error: ${e.message}`)
            }
        }
    })

}

await fetchArticles()